# Project shops - prices 

> Note: Данный проект находится в разработке

## Используемые технологии:
* [React](https://reactjs.org/)
* [TypeScript](https://www.typescriptlang.org/)
* [React bootstrap](https://react-bootstrap.netlify.app/)
* [ApexCharts](https://apexcharts.com/)
* Backend от [@Irelynx](https://github.com/Irelynx)
  * Node.js (Express, TypeScript, Sequelize, Postgres)


## Скриншоты:

### Каталог
![Catalog](docs/Catalog.png)

### Авторизация
![Authorization](docs/Auth.png)


### Регистрация
![Register](docs/Register.png)

### Карточка товара
![Register](docs/Product.png)

